//
//  MyScene.swift
//  Watch_Demo WatchKit Extension
//
//  Created by Chia-Feng Chuang on 2019/4/15.
//  Copyright © 2019 Chia-Feng Chuang. All rights reserved.
//

import Foundation
import SpriteKit

class myScene: SKScene {
    var background = SKSpriteNode(imageNamed: "1")
    
    override func sceneDidLoad() {
        self.scaleMode = .aspectFill
        background.position = CGPoint(x: self.size.height/2, y: self.size.width/2)
        background.aspectFitToSize(fillSize: self.size)
        
        addChild(background)
    }
    
    func updateImage(name: String) {
        guard UIImage(named: name) != nil else { return }
            
        let newBackground = SKSpriteNode(imageNamed: name)
        newBackground.position = CGPoint(x: self.size.height/2, y: self.size.width/2)
        newBackground.aspectFitToSize(fillSize: self.size)
        removeChildren(in: [background])
        addChild(newBackground)
        background = newBackground
    }
}

extension SKSpriteNode {
    func aspectFillToSize(fillSize: CGSize) {
        if texture != nil {
            self.size = texture!.size()
            let verticalRatio = fillSize.height / self.texture!.size().height
            let horizontalRatio = fillSize.width / self.texture!.size().width
            
            let scaleRatio = horizontalRatio > verticalRatio ? horizontalRatio : verticalRatio
            
            self.setScale(scaleRatio)
        }
    }
    
    func aspectFitToSize(fillSize: CGSize) {
        if texture != nil {
            self.size = texture!.size()
            let verticalRatio = fillSize.height / self.texture!.size().height
            let horizontalRatio = fillSize.width / self.texture!.size().width
            
            let scaleRatio = horizontalRatio < verticalRatio ? horizontalRatio : verticalRatio
            
            self.setScale(scaleRatio)
        }
    }
}
