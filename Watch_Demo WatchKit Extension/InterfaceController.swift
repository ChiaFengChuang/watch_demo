//
//  InterfaceController.swift
//  Watch_Demo WatchKit Extension
//
//  Created by Chia-Feng Chuang on 2019/4/10.
//  Copyright © 2019 Chia-Feng Chuang. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var sceneInterface: WKInterfaceSKScene!
    var scene: myScene!
    
    let watchSessionManager = WatchSessionManager()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveMessage(_:)), name: .WCDidReceiveMessage, object: nil)
        

        scene = myScene()
        sceneInterface.presentScene(scene)
    }
    
    @objc
    func didReceiveMessage(_ notification: Notification) {
        guard let name = notification.object as? String else { return }
        
        DispatchQueue.main.async() {
            self.scene.updateImage(name: name)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    

}
