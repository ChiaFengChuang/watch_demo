//
//  WatchSessionManager.swift
//  Watch_Demo
//
//  Created by Chia-Feng Chuang on 2019/4/10.
//  Copyright © 2019 Chia-Feng Chuang. All rights reserved.
//

import WatchKit
import WatchConnectivity

extension Notification.Name {
    static let reachabilityChange = Notification.Name("reachabilityChange")
    static let WCDidReceiveMessage = Notification.Name("WCDidReceiveMessage")
}

class WatchSessionManager: NSObject, WCSessionDelegate {
    
    #if os(iOS)
    let deviceName = "iPhone"
    #else
    let deviceName = "Watch"
    #endif
    
    override init() {
        super.init()
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
    }
    
    func sendMessage(msg: String) {
        let dictData = ["No": msg]
        
        WCSession.default.sendMessage(dictData, replyHandler: { replyMessage in
            print("[\(self.deviceName)] onSendMessageBtnPressed: Get replied message")
        }) { error in
            print("[\(self.deviceName)] onSendMessageBtnPressed: Get Error")
        }
    }
    
    // WCSessionDelegate
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if activationState == .activated {
            print("[\(deviceName)] activationDidCompleteWith: activated")
            NotificationCenter.default.post(name: .reachabilityChange, object: WCSession.default.isReachable)
        } else {
            print("[\(deviceName)] activationDidCompleteWith: NOT activated")
        }
    }
    
    // WCSessionDelegate methods for iOS only.
    #if os(iOS)
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("[\(deviceName)] sessionDidBecomeInactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("[\(deviceName)] sessionDidDeactivate")
    }
    #endif
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        NotificationCenter.default.post(name: .reachabilityChange, object: WCSession.default.isReachable)
    }
    
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        print("[\(deviceName)] didReceiveMessage")
        NotificationCenter.default.post(name: .WCDidReceiveMessage, object: message["No"] as! String)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        print("[\(deviceName)] didReceiveMessage(reply)")
        NotificationCenter.default.post(name: .WCDidReceiveMessage, object: message["No"] as! String)
    }

}
