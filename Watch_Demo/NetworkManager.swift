//
//  NetworkManager.swift
//  Watch_Demo
//
//  Created by Chia-Feng Chuang on 2019/4/15.
//  Copyright © 2019 Chia-Feng Chuang. All rights reserved.
//

import CocoaAsyncSocket

extension Notification.Name {
    static let serverConnectedChange = Notification.Name("ServerConnectedChange")
    static let didReceiveMessage = Notification.Name("DidReceiveMessage")
}

class NetworkManager: NSObject {
    var socket: GCDAsyncSocket?
    //    let HOST_IP = "140.112.91.208"
    let DEFAULT_HOST_IP = "137.135.79.53"
    let DEFAULT_HOST_PORT: UInt16 = 20194
    private var HOST_IP: String?
    private var HOST_PORT: UInt16?
    private var _isServerConnected: Bool!
    private(set) var isServerConnected: Bool {
        get {
            return _isServerConnected
        }
        
        set {
            _isServerConnected = newValue
            NotificationCenter.default.post(name: .serverConnectedChange, object: _isServerConnected)
        }
    }
    var serverConnectTimer: Timer?
    
    override init() {
        super.init()
        isServerConnected = false
        socket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
    }
    
    
    @objc
    func connectServer(ip: String?, port: String?) {
        
        if let socket = socket {
            if socket.isConnected {
                socket.disconnect()
            }
            
            do {
                if let ip = ip, let port = port {
                    HOST_IP = ip
                    HOST_PORT = UInt16(port)
                } else {
                    HOST_IP = DEFAULT_HOST_IP
                    HOST_PORT = DEFAULT_HOST_PORT
                }
                try socket.connect(toHost: HOST_IP!, onPort: HOST_PORT!)
            } catch _ {
                print("socket failed")
            }
        }
    }
    
    func disconnect() {
        if let socket = socket, isServerConnected {
            socket.disconnect()
        }
    }
    
}

extension NetworkManager: GCDAsyncSocketDelegate {
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        print("[iPhone] didConnectToHost")
        socket?.write("d phone\r\n".data(using: String.Encoding.utf8)!, withTimeout: -1, tag: 0)
        socket?.readData(withTimeout: -1, tag: 0)
        isServerConnected = true
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        if let raw_msg = String(data: data, encoding: String.Encoding.utf8){
            if let idx = raw_msg.index(of: "\r\n") {
                let msg = String(raw_msg.prefix(upTo: idx))
                print("[iPhone] didRead: \(msg)")
                NotificationCenter.default.post(name: .didReceiveMessage, object: msg)
            }
        }
        socket?.readData(withTimeout: -1, tag: 0)
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        print("[iPhone] didDisconnect")
        isServerConnected = false
    }
}
