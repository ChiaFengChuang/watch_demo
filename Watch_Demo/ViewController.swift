//
//  ViewController.swift
//  Watch_Demo
//
//  Created by Chia-Feng Chuang on 2019/4/10.
//  Copyright © 2019 Chia-Feng Chuang. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lbl_watch: UILabel!
    @IBOutlet weak var lbl_server: UILabel!
    @IBOutlet weak var tv_log: UITextView!
    
    @IBOutlet weak var tf_ip: UITextField!
    @IBOutlet weak var tf_port: UITextField!
    
    let networkManager = NetworkManager()
    let watchSessionManager = WatchSessionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(serverConnectedChange(_:)), name: .serverConnectedChange, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveMessage(_:)), name: .didReceiveMessage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(watchReachabilityChange(_:)), name: .reachabilityChange, object: nil)
        
        
        self.tv_log.text = ""
        self.hideKeyboardWhenTappedAround()
    }
    
    
    
    // Notification Handle
    @objc
    private func serverConnectedChange(_ notification: Notification) {
        guard let isServerConnected = notification.object as? Bool else { return }
        
        DispatchQueue.main.async() {
            self.lbl_server.backgroundColor = isServerConnected ? .green : .red
            self.logInTextView(msg: "# Server " + (isServerConnected ? "Connected" : "Disconnected"))
        }
    }
    
    @objc
    private func didReceiveMessage(_ notification: Notification) {
        guard let msg = notification.object as? String else { return }
        
        logInTextView(msg: "Received message: \"\(msg)\"")
        watchSessionManager.sendMessage(msg: msg)
    }
    
    @objc
    private func watchReachabilityChange(_ notification: Notification) {
        guard let isReachable = notification.object as? Bool else { return }
        
        DispatchQueue.main.async() {
            self.lbl_watch.backgroundColor = isReachable ? .green : .red
            self.logInTextView(msg: "# Watch " + (isReachable ? "Connected" : "Disconnected"))
        }
    }

    // UI
    @IBAction func onConnectBtnPressed(_ sender: Any) {
        networkManager.connectServer(ip: tf_ip.text, port: tf_port.text)
    }
    
    @IBAction func onSendMessageBtnPressed(_ sender: UIButton) {
        watchSessionManager.sendMessage(msg: String(sender.tag))
    }
    
    @IBAction func onClearBtnPressed(_ sender: Any) {
        tv_log.text = ""
    }
    
    private func logInTextView(msg: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        let timeString = dateFormatter.string(from: Date())
        
        DispatchQueue.main.async() {
            self.tv_log.text = self.tv_log.text! + "[" + timeString + "] " + msg
             + "\n"
            self.tv_log.scrollRangeToVisible(NSRange(location: self.tv_log.text.count, length: 1))
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    deinit {
        networkManager.disconnect()
    }
}
